﻿using Oracle.ManagedDataAccess.Client;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using TM.AppUtils.Models;

namespace documents_manager.Models
{
    public class DataManager : BaseDataManager
    {
        public OracleConnection EDocuments { get; private set; }
        public OracleConnection VMDocuments { get; private set; }
        

        public DataManager(Dictionary<string, string> connectionStrings)
        {
            VMDocuments = new OracleConnection(connectionStrings["vmdocuments"]);
            EDocuments = new OracleConnection(connectionStrings["edocuments"]);
        }
    }
}
