﻿using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Types;
using documents_manager.Core;
using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using TM.AppUtils.Models;

namespace documents_manager.Models.Repositories
{
    public class DataRepository : BaseRepository
    {
        protected new DataManager dm { get { return base.dm as DataManager; } }
        public DataTable GetDocuments(int type, DateTime dateFrom, DateTime dateTo, string barcode, string q, string qtype)
        {
            DataTable source = new DataTable();
            OracleCommand cmd = dm.EDocuments.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pk_statements.return_statements3";
            cmd.Parameters.Add("in_orders", OracleDbType.Varchar2, ParameterDirection.Input).Value = null; //      in varchar2,
            cmd.Parameters.Add("in_designation", OracleDbType.Varchar2, ParameterDirection.Input).Value = null; // in varchar2,
            cmd.Parameters.Add("in_date_begin", OracleDbType.Date, ParameterDirection.Input).Value = dateFrom; //  in date,
            cmd.Parameters.Add("in_date_end", OracleDbType.Date, ParameterDirection.Input).Value = dateTo; //    in date,
            cmd.Parameters.Add("in_id", OracleDbType.Int32, ParameterDirection.Input).Value = 0; //          in number,
            cmd.Parameters.Add("in_project_id", OracleDbType.Int32, ParameterDirection.Input).Value = null; //  in number,
            cmd.Parameters.Add("in_is_scan", OracleDbType.Int32, ParameterDirection.Input).Value = null; //     in number,
            cmd.Parameters.Add("in_number", OracleDbType.Varchar2, ParameterDirection.Input).Value = null; //      in varchar2,
            cmd.Parameters.Add("in_name", OracleDbType.Varchar2, ParameterDirection.Input).Value = null; //        in varchar2,
            cmd.Parameters.Add("in_barcode", OracleDbType.Varchar2, ParameterDirection.Input).Value = barcode; //     in varchar2,
            cmd.Parameters.Add("RESULT", OracleDbType.RefCursor, ParameterDirection.Output);

            if (!String.IsNullOrEmpty(q))
                cmd.Parameters[qtype].Value = q;

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();

            source.Load(((OracleRefCursor)cmd.Parameters["RESULT"].Value).GetDataReader());
            cmd.Connection.Close();

            if (source.Rows.Count > 0)
            {
                Dictionary<int, int[]> typings = new Dictionary<int, int[]>
                {
                    { 0, new int[] { 3, 14, 20 }},
                    { 1, new int[] { 3, 20 }},
                    { 2, new int[] { 14 }}
                };

                source.Columns.Add("_ID", typeof(Int64));
                source.Columns.Add("_PARENT_ID", typeof(Int64));
                source.Columns.Add("_PARENT_BARCODE", typeof(Int32));

                List<DataRow> _tmp = source.AsEnumerable().Where(x =>
                {
                    int id = (int)x.Field<long>("ID");
                    int parent_id = (int)x.Field<decimal>("PARENT_ID");
                    DataRow[] parent = source.Select($"ID={parent_id}");
                    if (id == parent_id || parent.Length == 0)
                        x["PARENT_ID"] = DBNull.Value;
                    else if (parent.Length > 0 && id != parent_id)
                        x["_PARENT_BARCODE"] = parent[0]["BARCODE"];

                    x["_ID"] = x["ID"];
                    x["_PARENT_ID"] = x["PARENT_ID"];

                    int t = Convert.ToInt32(x["TYPE"]);
                    if (type == 0)
                        return !typings[type].Contains(t);

                    return typings[type].Contains(t);
                }).ToList();

                _tmp.ForEach(x =>
                {
                    x["_ID"] = x["_ID"].ToString() + x["BARCODE"].ToString();
                    if (!x.IsNull("_PARENT_ID"))
                        x["_PARENT_ID"] = x["_PARENT_ID"].ToString() + x["_PARENT_BARCODE"].ToString();
                });

                if (_tmp.Count > 0)
                    return _tmp.CopyToDataTable();
            }

            return new DataTable();
        }

        public DataTable GetVMDocuments(int id, DateTime dateFrom, DateTime dateTo, string q, string qtype)
        {
            DataTable source = new DataTable();
            OracleCommand cmd = dm.VMDocuments.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pk_electronic_agreement.return_list_vm";
            cmd.Parameters.Add("in_designation", OracleDbType.Varchar2, ParameterDirection.Input);
            cmd.Parameters.Add("in_dep_id", OracleDbType.Int32, ParameterDirection.Input).Value = DBNull.Value;
            cmd.Parameters.Add("in_date_begin", OracleDbType.Date, ParameterDirection.Input).Value = dateFrom;
            cmd.Parameters.Add("in_date_end", OracleDbType.Date, ParameterDirection.Input).Value = dateTo;
            cmd.Parameters.Add("in_barcode", OracleDbType.Int32, ParameterDirection.Input);
            cmd.Parameters.Add("in_order_code", OracleDbType.Int32, ParameterDirection.Input);
            cmd.Parameters.Add("in_id_st", OracleDbType.Int32, ParameterDirection.Input).Value = DBNull.Value;
            cmd.Parameters.Add("in_change_designation", OracleDbType.Varchar2, ParameterDirection.Input);
            cmd.Parameters.Add("RESULT", OracleDbType.RefCursor, ParameterDirection.Output);


            if (id > 0)
                cmd.Parameters["in_id_st"].Value = id;

            if (!String.IsNullOrEmpty(q))
                cmd.Parameters[qtype].Value = q;

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();

            source.Load(((OracleRefCursor)cmd.Parameters["RESULT"].Value).GetDataReader());
            cmd.Connection.Close();

            if (source.Rows.Count == 0)
                return new DataTable();

            source.Columns["IMG_INDEX"].ColumnName = "TYPE";

            return source;
        }

        /// <summary>
        /// Возврщает Расширенную информацию о документе
        /// </summary>
        /// <param name="id">Идентификатор документа</param>
        /// <param name="type">Тип документа</param>
        /// <returns>Расширенная информация о документе</returns>
        public DataTable GetDocumentExtra(int id, int type)
        {
            DataTable source = new DataTable();

            OracleCommand cmd = dm.EDocuments.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pk_statements.return_statement_extra_info";
            cmd.Parameters.Add("in_id", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("in_type", OracleDbType.Int32, ParameterDirection.Input).Value = type;
            cmd.Parameters.Add("RESULT", OracleDbType.RefCursor, ParameterDirection.Output);

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            source.Load(((OracleRefCursor)cmd.Parameters["RESULT"].Value).GetDataReader());
            cmd.Connection.Close();

            return source;
        }

        public byte[] GetDocumentFile(int id, int type, int barcode, string workshop, out string filename)
        {
            string path = "";
            byte[] file = new byte[0];

            OracleCommand cmd = dm.EDocuments.CreateCommand();
            cmd.CommandType = CommandType.Text;
            cmd.CommandText = "select tvs.srv_share_unit || '\\Checked In\\' || pk_statements_services.get_statement_filename2(:sid, :stype, :sbarcode, :sworkshop) from smarteam.tdm_vault_servers tvs";
            cmd.Parameters.Add("sid", id);
            cmd.Parameters.Add("stype", type);
            cmd.Parameters.Add("sbarcode", barcode);
            cmd.Parameters.Add("sworkshop", workshop);

            cmd.Connection.Open();
            path = cmd.ExecuteScalar()?.ToString();
            cmd.Connection.Close();
            if (String.IsNullOrEmpty(Path.GetFileName(path)))
                throw new FileNotFoundException();
            using (NetworkConnection nc = new NetworkConnection(Path.GetPathRoot(path), new NetworkCredential(@"fs5\DocumentsStorage", "user@2017!")))
            {
                nc.Close().Authorize();
                string[] files = Directory.GetFiles(Path.GetPathRoot(path), "*.*");
                file = File.ReadAllBytes(path);
                filename = Path.GetFileName(path);
            }

            return file;
        }

        /// <summary>
        /// Возврщает состав документа
        /// </summary>
        /// <param name="id">Идентификатор документа</param>
        /// <param name="type">Тип документа</param>
        /// <returns>Состав документа</returns>
        public DataTable GetDocumentComposition(int id, int type)
        {
            DataTable source = new DataTable();

            OracleCommand cmd = dm.EDocuments.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pk_statements.return_statements_tree";
            cmd.Parameters.Add("in_id", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("in_type", OracleDbType.Int32, ParameterDirection.Input).Value = type;
            cmd.Parameters.Add("RESULT", OracleDbType.RefCursor, ParameterDirection.Output);

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            source.Load(((OracleRefCursor)cmd.Parameters["RESULT"].Value).GetDataReader());
            cmd.Connection.Close();

            if (source.Rows.Count > 0)
            {
                source = source.AsEnumerable().Select(x =>
                {
                    if (Convert.ToInt32(x["ID"]) == Convert.ToInt32(x["PARENT_ID"]))
                        x["PARENT_ID"] = DBNull.Value;
                    return x;
                }).CopyToDataTable();
            }

            return source;
        }

        public DataTable GetDocumentVMComposition(int project_id)
        {
            DataTable source = new DataTable();

            OracleCommand cmd = dm.VMDocuments.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pk_electronic_agreement.return_documents ";
            cmd.Parameters.Add("in_project_id", OracleDbType.Int32, ParameterDirection.Input).Value = project_id;
            cmd.Parameters.Add("RESULT", OracleDbType.RefCursor, ParameterDirection.Output);

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            source.Load(((OracleRefCursor)cmd.Parameters["RESULT"].Value).GetDataReader());
            cmd.Connection.Close();

            if (source.Rows.Count == 0)
                return new DataTable();

            source.Columns["IMG_INDEX"].ColumnName = "TYPE";

            source = source.AsEnumerable().Select(x =>
            {
                if (!x.IsNull("PARENT_ID") && source.Select($"ID={x["PARENT_ID"]}").Length == 0)
                    x["PARENT_ID"] = DBNull.Value;
                return x;
            }).CopyToDataTable();

            return source;
        }

        /// <summary>
        /// Возвращает включенные/исключенные документы
        /// </summary>
        /// <param name="id">Идентификатор документа</param>
        /// <param name="type">Тип документа</param>
        /// <returns>Включенные/исключенные документы документа</returns>
        public DataTable GetDocumentExcludes(int id, int type)
        {
            DataTable source = new DataTable();

            OracleCommand cmd = dm.EDocuments.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pk_statements.return_statements_exclude2";
            cmd.Parameters.Add("in_id", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("in_type", OracleDbType.Int32, ParameterDirection.Input).Value = type;
            cmd.Parameters.Add("RESULT", OracleDbType.RefCursor, ParameterDirection.Output);

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            source.Load(((OracleRefCursor)cmd.Parameters["RESULT"].Value).GetDataReader());
            cmd.Connection.Close();

            if (source.Rows.Count > 0)
            {
                source = source.AsEnumerable().Select(x =>
                {
                    if (Convert.ToInt32(x["ID"]) == Convert.ToInt32(x["PARENT_ID"]))
                        x["PARENT_ID"] = DBNull.Value;
                    return x;
                }).CopyToDataTable();
            }

            return source;
        }

        /// <summary>
        /// Возвращает Аннулированные ВП
        /// </summary>
        /// <param name="id">Идентификатор документа</param>
        /// <param name="type">Тип документа</param>
        /// <returns>Аннулированные ВП</returns>
        public DataTable GetDocumentCanceledVP(int id, int type)
        {
            DataTable source = new DataTable();

            OracleCommand cmd = dm.EDocuments.CreateCommand();
            cmd.CommandType = CommandType.StoredProcedure;
            cmd.CommandText = "pk_statements.return_statements_history2";
            cmd.Parameters.Add("in_id", OracleDbType.Int32, ParameterDirection.Input).Value = id;
            cmd.Parameters.Add("in_type", OracleDbType.Int32, ParameterDirection.Input).Value = type;
            cmd.Parameters.Add("RESULT", OracleDbType.RefCursor, ParameterDirection.Output);

            cmd.Connection.Open();
            cmd.ExecuteNonQuery();
            source.Load(((OracleRefCursor)cmd.Parameters["RESULT"].Value).GetDataReader());
            cmd.Connection.Close();

            return source;
        }
    }
}
