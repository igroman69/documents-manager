﻿var path = require("path");
var webpack = require("webpack");
const UglifyJSPlugin = require("uglifyjs-webpack-plugin");

module.exports = {
    entry: {
        "bundle": "./wwwroot/src/main"
    },
    output: {
        path: path.resolve(__dirname, "wwwroot/dist/"),
        filename: "[name].js"
    },
    //devtool: "source-map",
    resolve: {
        extensions: [".ts", ".js"]
    },
    module: {
        loaders: [
            { test: /\.ts$/, loader: "ts-loader" }
        ]
    },
    plugins: [
        new webpack.ProvidePlugin({
            $: 'jquery',
            jQuery: 'jquery',
            'window.jQuery': 'jquery'
        }),
        new webpack.optimize.CommonsChunkPlugin({
            name: "vendor",
            filename: "vendor.bundle.js",
            minChunks: function (module, count) {
                let ctx = module.context;
                if (typeof ctx !== 'string')
                    return false;
                return ctx.indexOf('node_modules') > -1;
            }
        }),
        //new webpack.SourceMapDevToolPlugin({
        //    //filename: '[name].js.map',
        //    exclude: ['vendor.bundle.js']
        //}),
        new UglifyJSPlugin({
            compress: {
                warnings: false
            },
            mangle: false,
            exclude: "bundle\.js"
        })
    ]
}
