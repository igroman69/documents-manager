﻿import * as jQuery from 'jquery';
import * as ng from 'angular';
import * as UIRouter from '@uirouter/angularjs';
import '@uirouter/angularjs';
import 'kendo-ui/js/kendo.web';
import 'kendo-ui/js/cultures/kendo.culture.ru';
import 'kendo-ui/js/messages/kendo.messages.ru-RU';

import './init'; // <<< Главный файл инициализации содержит все правки на старте для последующего TypeDefinitions

import { AppStateService } from './services/app-state.service';
import { ColumnsCompilerService } from './services/columns-compiler.service';
import { DataSourceService } from './services/data-source.service';

import { AppComponent } from './app.component';
import { ToolbarComponent } from './components/toolbar.component';
import { DocumentsComponent } from './components/documents.component';
import { DocumentDetailsComponent } from './components/document-details.component';
import { DocumentAdditionalComponent } from './components/document-additional.component';

/******************************************************************************/
/*************** Плагин для создания Dom-элементов KendoUI ********************/
/******************************************************************************/
$.kendoDom = (n: string, o: any) => {
    let re = /(?!^)([A-Z]{1})/g;
    let tpl = '<{0} {1}></{0}>';

    let prms = '';
    for (let k in o) {
        prms += ' ';
        prms += k.replace(re, '-$1').toLowerCase();
        if (o[k])
            prms += '="' + o[k] + '"';
    }
    return kendo.format(tpl, n, $.trim(prms));
}
/********************************** End **************************************/

export const app: ng.IModule = ng.module('DocumentsManager', ['ui.router', 'kendo.directives']);

app.run(($rootScope: ng.IRootScopeService) => {
    kendo.culture("ru");
});

app.config(['$stateProvider', '$urlRouterProvider', ($stateProvider: UIRouter.StateProvider, $urlRouterProvider: UIRouter.UrlRouterProvider) => {
    $stateProvider.state("documents", {
        url: '/documents/{type:[0-3]}?(id|barcode|params)',
        component: 'documents'
    });

    // Вернусь позже к этому
    //$stateProvider.state("documents.params", {
    //    abstract: false,
    //    url: '?(id|barcode|q)',
    //    component: 'documents',
    //})

    $urlRouterProvider.when('', '/documents/0');
}]);

app.filter("replace", () => {
    // Фильтр замены символов через шаблон
    return (input: string, x: string, y: string) => {
        if (input === undefined)
            return input;
        return input.replace(x, y);
    };
});

app.filter("format", ($sce: ng.ISCEService) => {
    return (input, format: string) => {
        // Фильтр Форматирования через шаблон
        if (input === undefined || input === null || input.toString().trim() === "")
            return $sce.trustAsHtml("&#8213");

        if (format !== undefined && format !== "") {
            if (!isNaN(Date.parse(input)))
                input = new Date(input);
            input = kendo.format(format, input);
        }

        return $sce.trustAsHtml(input);
    };
});

app.service("$appState", AppStateService)
    .service("$columnsCompiler", ColumnsCompilerService)
    .service("$dataSource", DataSourceService)
    .component("app", new AppComponent())
    .component("toolbar", new ToolbarComponent())
    .component("documents", new DocumentsComponent())
    .component("documentDetails", new DocumentDetailsComponent())
    .component("documentAdditional", new DocumentAdditionalComponent());