﻿import { Subject, Subscription } from 'rxjs';

export class ObservableObjectEvent {
    public field: string;
    public value: any;
    public timestamp: Date;

    constructor(params?: any) {
        for (let k in params) {
            this[k] = params[k];
        }
        this.timestamp = new Date();
    }
}

export class ObservableObjectSetEvent extends ObservableObjectEvent {
    public oldValue: any;
    constructor(params?: any) {
        super(params);
    }
}

export class ObservableObject {
    private __setEvents: Subject<ObservableObjectSetEvent>;
    private __getEvents: Subject<ObservableObjectEvent>;
    private __changeEvents: Subject<ObservableObjectSetEvent>;
    private __keys: Array<string>;
    constructor(data: any) {
        this.__keys = [];
        this.__getEvents = new Subject();
        this.__setEvents = new Subject();
        this.__changeEvents = new Subject();

        for (let k in data) {
            this[k] = data[k];
            this.__keys.push(k);
        }
    }

    get(name: string): any {
        this.__getEvents.next(new ObservableObjectEvent({ field: name, value: this[name] }));
        return this[name];
    }

    set(name: string, value: any): void {
        let eventObject: ObservableObjectSetEvent = new ObservableObjectSetEvent({ field: name, value: value, oldValue: this[name] });
        this[name] = value;
        if (this.__keys.indexOf(name) === -1)
            this.__keys.push(name);
        this.__setEvents.next(eventObject);
        this.__changeEvents.next(eventObject);
    }

    subscribe(eventName: string, fn: Function): Subscription {
        return (this[`__${eventName}Events`] as Subject<any>).subscribe(
            v => fn(v),
            error => console.error(error)
        );
    }

    trigger(eventName: string) {
        (this[`__${eventName}Events`] as Subject<any>).next(new ObservableObjectEvent());
    }

    toJSON(): any {
        let json: any = {};
        this.__keys.forEach(v => {
            //if (!isNaN(Date.parse(this[v])) && isNaN(Number(this[v])))
            //    json[v] = new Date(this[v]);
            //else
                json[v] = this[v];
        });
        return json;
    }
}