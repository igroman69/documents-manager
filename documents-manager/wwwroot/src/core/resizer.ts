﻿
let running: boolean = false;
window.addEventListener("resize", () => {
    if (running) return;
    running = true;
    requestAnimationFrame(() => {
        window.dispatchEvent(new CustomEvent('optimizedResize'));
        running = false;
    });
});

export interface IResizerOptions {
    resizeBy: JQuery;
    element: JQuery;
    isWatch?: boolean;
    callback?: Function
}

export class Resizer {
    constructor(public options: IResizerOptions) {
        options.isWatch = options.isWatch || false;
        if (options.isWatch) {
            window.addEventListener("optimizedResize", () => this.resize());
            if (options.callback)
                window.addEventListener("optimizedResize", () => options.callback());
        }
        this.resize();
    }

    public resize() {
        this.height();
    }

    private height() {
        let sizes = $.makeArray(this.sizes('height'));
        sizes.push(parseFloat(this.options.element.css("margin-top")));
        sizes.push(parseFloat(this.options.element.css("margin-bottom")));
        this.options.element.outerHeight(this.options.resizeBy.innerHeight() - eval(sizes.join('+')));
    }

    private get filteredChildren(): JQuery {
        let parents = this.options.element.parentsUntil(this.options.resizeBy);
        parents = parents.add(this.options.resizeBy);

        return parents.children().filter((i: number, e: Element) => {
            // не является самим элементом и не является родителем по прямой ветке
            return !$(e).is(this.options.element) && !parents.is(e) && !$(e).hasClass('k-loading-mask');
        });
    }

    private sizes(type: string): JQuery {
        return this.filteredChildren.map((i: number, e: Element) => {
            return type === 'height' ? $(e).outerHeight(true) : $(e).outerWidth(true);
        });
    }
}