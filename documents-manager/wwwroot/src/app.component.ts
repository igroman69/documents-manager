﻿import * as ng from 'angular';
import * as UIRouter from '@uirouter/angularjs';

import { AppStateService } from './services/app-state.service';


export class AppController {
    public static $inject = [
        '$scope',
        '$state',
        '$stateParams',
        '$appState'
    ];

    public options: any;

    //Scopes
    public tabsScope: kendo.ui.TabStrip;
    public notificationScope: kendo.ui.Notification;

    constructor(private $scope: ng.IScope,
        private $state: UIRouter.StateService,
        private $stateParams: UIRouter.StateParams,
        private $appState: AppStateService) {

        this.options = {
            tabs: {
                value: this.$appState.tabs[$appState.filter.get('type')].caption,
                dataTextField: 'caption',
                tabPosition: 'left',
                dataSource: this.$appState.tabs,
                select: (e: kendo.ui.TabStripSelectEvent) => {
                    let idx: number = $(e.item).parent().children().index(e.item);
                    let oldi: number = Number(this.$appState.filter.get("type"));
                    if (idx !== oldi)
                        this.$appState.filter.set("type", idx);
                }
            } as kendo.ui.TabStripOptions
        };
    }

    notify(message, type): void {
        this.notificationScope.show(message, type);
    }
}

export class AppComponent implements ng.IComponentOptions {
    public controller: any = AppController;
    public templateUrl: string = 'templates/app.template.html';
    public transclude: boolean = true;
}