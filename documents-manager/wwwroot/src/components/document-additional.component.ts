﻿import * as ng from 'angular';

import { AppController } from '../app.component';
import { AppStateService } from '../services/app-state.service';
import { DataSourceService } from '../services/data-source.service';
import { ColumnsCompilerService } from '../services/columns-compiler.service';
import { Resizer } from '../core/resizer';

export class DocumentAdditionalController {
    public $inject = [
        '$scope', '$element', '$http', '$appState', '$dataSource', '$columnsCompiler'
    ];

    public type: number;
    public options: any;
    public dataSources: any;

    //Scopes
    public app: AppController;
    public model: any;
    public tabsScope: kendo.ui.TabStrip;
    public listScopes: Array<kendo.ui.Widget>;

    //Scope Events
    private selectedChangeEvent;

    constructor(private $scope: ng.IScope, private $element: ng.IRootElementService,
        private $http: ng.IHttpService, private $appState: AppStateService, private $dataSource: DataSourceService,
        private $columnsCompiler: ColumnsCompilerService) {

        this.type = $appState.filter.get("type");

        this.dataSources = {
            composition: this.buildDataSource("composition"),
            compositionVM: this.buildDataSource("compositionVM"),
            excluded: this.buildDataSource("excluded"),
            canceled: this.buildDataSource("canceled")
        };

        let tabResizer;
        $scope.$on("kendoWidgetCreated", (e: ng.IAngularEvent, widget: kendo.ui.Widget) => {
            if (widget === this.tabsScope) {
                //this.wrappersScopes = [this.compositionScope.wrapper, this.excludedScope.wrapper, this.canceledScope.wrapper];
                tabResizer = new Resizer({
                    resizeBy: this.$element,
                    element: this.tabsScope.wrapper.children('.k-content'),
                    isWatch: true,
                    callback: () => {
                        //console.log('resized');
                    }
                });
            }
        });

        this.options = {
            composition: (): kendo.ui.TreeListOptions => {
                return {
                    dataSource: this.dataSources.composition,
                    height: 233,
                    columns: [
                        { template: '<i class="icon-type-#=TYPE#"></i>', width: 70 },
                        { field: 'BARCODE', title: 'Штрихкод', width: 135 },
                        { field: 'NUMBERS', title: 'Номер', width: 75 },
                        { template: '<i class="icon-class-#=CLASS_ID#"></i>', width: 70 },
                        { field: 'DESIGNATION', title: 'Обозначение' },
                        { field: 'NAME', title: 'Наименование' },
                        { field: 'SCAN_DATE', title: 'Дата сканирования', format: '{0:dd.MM.yyyy HH:mm:ss}', width: 130 }
                    ]
                }
            },

            compositionVM: (): kendo.ui.TreeListOptions => {
                return {
                    dataSource: this.dataSources.compositionVM,
                    height: 233,
                    columns: this.$columnsCompiler.compile(3),
                    dataBound: (e: kendo.ui.TreeListDataBoundEvent) => {
                        window["tlist"] = e.sender;
                    }
                }
            },

            excluded: (): kendo.ui.TreeListOptions => {
                return {
                    dataSource: this.dataSources.excluded,
                    height: 233,
                    columns: [
                        { template: '<i class="icon-class-#=CLASS_ID#"></i>', width: 70 },
                        { field: 'DESIGNATION', title: 'Обозначение', width: 200 },
                        { field: 'REASON', title: 'Причина', width: 120 },
                        { template: '<i class="icon-type-#=TYPE#"></i>', width: 70 },
                        { field: 'BARCODE', title: 'Штрихкод', width: 135 },
                        { field: 'ST_DESIGNATION', title: 'Обозначение последнего расчета' },
                        { field: 'ST_NAME', title: 'Наименование', width: 120 },
                        { field: 'ORDERS', title: 'ШПЗ', width: 120 },
                        { field: 'SCAN_DATE', title: 'Дата сканирования', format: '{0:dd.MM.yyyy HH:mm:ss}', width: 130 }
                    ]
                }
            },

            canceled: (): kendo.ui.GridOptions => {
                return {
                    dataSource: this.dataSources.canceled,
                    height: 233,
                    columns: [
                        { template: '<i class="icon-type-#=TYPE#"></i>', width: 70 },
                        { field: 'BARCODE', title: 'Штрихкод', width: 135 },
                        { template: '<i class="icon-class-#=CLASS_ID#"></i>', width: 70 },
                        { field: 'DESIGNATION', title: 'Обозначение', width: 200 },
                        { field: 'NAME', title: 'Наименование' },
                        { field: 'STATE', title: 'Состояние', width: 120 },
                        { field: 'ST_REPLACEMENT_INSTEAD', title: 'Взамен', width: 120 },
                        { field: 'ST_REPLACEMENT_BY', title: 'Заменен на', width: 120 },
                        { field: 'NOTIFY', title: 'Извещение', width: 120 },
                        { field: 'SCAN_DATE', title: 'Дата сканирования', format: '{0:dd.MM.yyyy HH:mm:ss}', width: 130 },
                        { field: 'USERNAME', title: 'Пользователь', width: 120 }
                    ]
                }
            }
        };
    }

    dataFunc(): any {
        let st = this.$appState.selected.get("document");
        if (!st)
            return;
        return {
            id: st.ID,
            type: st.TYPE,
            project_id: st.PROJECT_ID,
            is_vm: this.type === 3
        }
    }

    buildDataSource(name: string): kendo.data.DataSource {
        let ds: any;
        switch (name) {
            case "composition":
            case "compositionVM":
            case "excluded":
                ds = this.$dataSource.render(kendo.data.TreeListDataSource, name, () => this.dataFunc());
                break;
            case "canceled":
                ds = this.$dataSource.render(kendo.data.DataSource, name, () => this.dataFunc());
                break;
        }

        ds.bind("requestStart", (e: kendo.data.DataSourceRequestStartEvent) => {
            kendo.ui.progress(this.$element, true);
        });
        ds.bind("requestEnd", (e: kendo.data.DataSourceRequestEndEvent) => {
            kendo.ui.progress(this.$element, false);
        });

        return ds;
    }

    CheckFileExists(e: JQueryEventObject): void {
        e.preventDefault();
        let t = $(e.currentTarget);
        let progressArea = t.closest('.pane')
        kendo.ui.progress(progressArea, true);
        $.get(t.attr('href'))
            .done((response: JQueryXHR) => {
                location.href = t.attr('href');
            })
            .fail((response: JQueryXHR) => {
                if (response.status === 404)
                    this.app.notify("Файл не найден!", "error");
            })
            .then(() => {
                kendo.ui.progress(progressArea, false);
            });
    }

    $onInit(): void {
        this.$element.css({ display: "inline-block", height: "300px", width: "100%" });

        this.selectedChangeEvent = this.$appState.selected.subscribe("change", e => {
            if (this.$appState.selected.get("document") !== null) {
                if (this.type === 0) {
                    this.dataSources.composition.read();
                    this.dataSources.excluded.read();
                } else if (this.type > 0 && this.type < 3) {
                    this.dataSources.canceled.read();
                } else if (this.type === 3) {
                    this.dataSources.compositionVM.read();
                }
            }
        });
    }
    $onDestroy(): void {
        this.selectedChangeEvent.unsubscribe();
    }
}

export class DocumentAdditionalComponent implements ng.IComponentOptions {
    public controller: any = DocumentAdditionalController;
    public templateUrl: string = 'templates/document-additional.template.html';
    public require = {
        app: '^app'
    }
}