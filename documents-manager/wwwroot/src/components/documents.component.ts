﻿import * as ng from 'angular';
import * as UIRouter from '@uirouter/angularjs';

import { AppController } from '../app.component';
import { AppStateService } from '../services/app-state.service';
import { ColumnsCompilerService } from '../services/columns-compiler.service';
import { DataSourceService } from '../services/data-source.service';

export class DocumentsController {
    public $inject = [
        '$scope',
        '$element',
        '$state',
        '$stateParams',
        '$appState',
        '$dataSource',
        '$columnsCompiler'
    ];

    public options: any;

    //Scopes
    public app: AppController;
    public appState: AppStateService;
    public listScope: kendo.ui.Widget;
    public listPagerScope: kendo.ui.Pager;

    public documentsDataSource: kendo.data.DataSource;
    private search = {
        value: "",
        fields: [],
        history:  []
    }

    constructor(private $scope: ng.IScope,
        private $element: ng.IRootElementService,
        private $state: UIRouter.StateService,
        private $stateParams: UIRouter.StateParams,
        private $appState: AppStateService,
        private $dataSource: DataSourceService,
        private $columnsCompiler: ColumnsCompilerService) {

        this.appState = $appState;

        if (this.appState.filter.get('type') === 3)
            this.documentsDataSource = $dataSource.render(kendo.data.DataSource, 'documentsVM', (options: any) => this.dataFunction(options));
        else
            this.documentsDataSource = $dataSource.render(kendo.data.TreeListDataSource, "documents", (options: any) => this.dataFunction(options));

        this.documentsDataSource.bind('error', (e: kendo.data.DataSourceErrorEvent) => {
            kendo.ui.progress(this.$element, false);
        })
        this.documentsDataSource.bind("requestStart", (e: kendo.data.DataSourceRequestStartEvent) => {
            kendo.ui.progress(this.$element, true);
        });
        this.documentsDataSource.bind("requestEnd", (e: kendo.data.DataSourceRequestEndEvent) => {
            kendo.ui.progress(this.$element, false);
            let r = e.response;
            if (r.qfiltertypes)
                this.$appState.qfiltertypes.data(e.response.qfiltertypes);

            /***** Для поиска *****/
            if (r.search && !r.search.message) {
                let s = r.search;
                this.search.fields = s.fields;
                if (this.search.history.indexOf(s.founded) === -1)
                    this.search.history.push(s.founded);
                if (s.redirect)
                    this.listPagerScope.page(s.page);
            } else if (r.search && r.search.message) {
                kendo.alert(r.search.message);
            }
            /**********************/
        });

        this.options = {
            splitter: {
                orientation: 'vertical',
                panes: [{ scrollable: false }, { size: '300px', max: '300px', collapsed: false, scrollable: false }],
                resize: (e: kendo.ui.SplitterEvent) => {
                    this.resize();
                }
            } as kendo.ui.SplitterOptions,

            // Всплывающие подсказки для TreeList
            listToolTip: {
                filter: "td[role=gridcell]",

                show: (e: kendo.ui.TooltipEvent) => {
                    let p = e.sender.popup.wrapper;
                    e.sender.popup.wrapper.addClass("s-custom-tooltips");
                },
                content: function (e: JQueryEventObject) {
                    let text = $.trim($(e.target).text());
                    if (text.length > 0)
                        return '<div style="width: ' + text.length * .6 + 'em">' + text + '</div>';
                    else
                        return;
                }
            } as kendo.ui.TooltipOptions,

            list: () => {
                return {
                    dataSource: this.documentsDataSource,
                    selectable: "single row",
                    columns: this.$columnsCompiler.compile(this.$appState.filter.get("type")),
                    dataBound: (e: any) => {
                        this.resize();
                        if (this.$appState.filter.get("qissearch"))
                            this.selectSearchValue();
                        if (e.sender.items().length === 1)
                            e.sender.select(e.sender.items().eq(0));
                    },
                    change: (e: any) => {
                        this.$appState.selected.set("document", e.sender.dataItem(e.sender.select()))
                        this.$scope.$apply();
                    }
                };
            }
        };
    }

    public resize(): void {
        if (this.listScope) {
            var pagerHeight = this.listScope.element.closest('.layer').next().height();
            var paneHeight = this.listScope.element.closest(".k-pane").height();
            this.listScope.element.css({ height: paneHeight - pagerHeight });
            this.listScope.resize();
        }
    }

    /**
     * Метод для выделения строки поиска в таблице данных
     */
    public selectSearchValue(): void {
        let q: string = this.$appState.filter.get('q');
        let lastID: number = this.search.history[this.search.history.length - 1];
        let styles: any = { false: "s-searched", true: "s-searched s-primary"}
        let columns: any[] = (this.listScope["columns"] as any[]).map(v => v.field);
        let rows: JQuery = this.listScope.element.children('.k-grid-content').find('tr[data-uid]:visible');

        // get only search fields columns index
        columns = this.search.fields.map(v => columns.indexOf(v));

        rows.each((idx: number, row: Element) => {
            let cells: JQuery = $(row).children('td[role=gridcell]');
            let item: any = this.listScope["dataItem"](row);
            let itemID: number = item["_ID"] || item["ID"];
            let isPrimary: boolean = itemID === lastID;
            let replacedFormat: string = `<span class="${styles[isPrimary.toString()]}">{0}</span>`;
            columns.forEach(columnIndex => {
                let cell = cells.eq(columnIndex);
                if (cell.children().length > 0) {
                    cell.find('*')
                        .filter((idx: number, el: Element) => $(el).children().length === 0)
                        .each((idx: number, el: Element) => this.HightlightSearchQueryValue($(el), q, replacedFormat));
                } else
                    this.HightlightSearchQueryValue(cell, q, replacedFormat);
            });
        });
        /************** scrolling panel ************/
        let container = this.listScope.element.children('.k-grid-content');
        let scrollTo = container.find('.s-searched.s-primary').closest('tr');
        if (scrollTo.length > 0) {
            container.animate({
                scrollTop: scrollTo.offset().top - container.offset().top + container.scrollTop()
            })
        }
        /*******************************************/

    }

    private HightlightSearchQueryValue(element: JQuery, q: string, replacedFormat: string): void {
        let v: string = element.html();
        let startIdx: number = v.toLowerCase().indexOf(q.toLowerCase());
        if (startIdx > -1) {
            let replacedValue: string = kendo.format(replacedFormat, v.slice(startIdx, startIdx + q.length));
            element.html(v.replace(new RegExp(q, "ig"), replacedValue));
        }
    }

    private stateChangeEvent: any;
    $onInit(): void {
        this.$element.css({ display: 'inline-block', height: '100%' });

        this.stateChangeEvent = this.$appState.filter.subscribe("change", e => {
            if (e.field !== 'type') {
                if (e.field === 'qissearch' && e.value === true) {
                    if (this.search.value !== this.appState.filter.get("q")) {
                        this.search.value = this.appState.filter.get("q");
                        this.search.history = [];
                    }
                    this.documentsDataSource.read({ q: this.appState.filter.get("q"), history: JSON.stringify(this.search.history) });
                }
                else {
                    this.search.value = "";
                    this.search.history = [];
                    this.listPagerScope.page(1);
                }
            }
        });
    }

    $onDestroy(): void {
        this.stateChangeEvent.unsubscribe();
    }

    public $templateUrl(): string {
        if (this.appState.filter.get('type') === 3)
            return 'templates/documents-vm.template.html';
        else
            return 'templates/documents.template.html';
    }

    private dataFunction(options: any): any {
        try {
            let filter = this.$appState.filter.toJSON();
            filter['dateFrom'] = filter['dateFrom'].toISOString();
            filter['dateTo'] = filter['dateTo'].toISOString();
            if (this.$stateParams['id'])
                filter['id'] = this.$stateParams['id'];
            if (this.$stateParams['barcode'])
                filter['barcode'] = this.$stateParams['barcode'];
            if (this.$stateParams['params']) {
                let q = JSON.parse(this.$stateParams['params']);
                for (let k in q)
                    filter[k] = !isNaN(Date.parse(q[k])) && isNaN(Number(q[k])) ? new Date(q[k]).toISOString() : q[k];
            }
            
            if (filter.qissearch)
                delete filter.q;

            return filter;
        } catch (err) {
            console.error(err);
        }
    }

    ToggleByBarcode(barcode: number): void {
        this.documentsDataSource.read({ barcode: barcode });
    }

    CheckFileExists(e: JQueryEventObject): void {
        e.preventDefault();
        let t = $(e.currentTarget);
        let progressArea = t.closest('.pane')
        kendo.ui.progress(progressArea, true);
        $.get(t.attr('href'))
            .done((response: JQueryXHR) => {
                location.href = t.attr('href');
            })
            .fail((response: JQueryXHR) => {
                if (response.status === 404)
                    this.app.notify("Файл не найден!", "error");
            })
            .then(() => {
                kendo.ui.progress(progressArea, false);
            });
    }
}

export class DocumentsComponent implements ng.IComponentOptions {
    public controller: any = DocumentsController;
    public template: string = '<div ng-include="$ctrl.$templateUrl()" style="display:inline-block; width: 100%; height: 100%;">'
    public require = {
        app: '^app'
    }
}