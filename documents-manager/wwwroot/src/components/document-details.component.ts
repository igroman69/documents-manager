﻿import * as ng from 'angular';

import { AppController } from '../app.component';
import { AppStateService } from '../services/app-state.service';

export class DocumentDetailsController {
    public $inject = [
        '$scope', '$element', '$http', '$appState'
    ];

    public options: any;

    //Scopes
    public app: AppController;
    public model: any;

    //Scope Events
    private selectedChangeEvent;

    constructor(private $scope: ng.IScope, private $element: ng.IRootElementService,
        private $http: ng.IHttpService, private $appState: AppStateService) {

        this.options = {

        };
    }

    $onInit(): void {
        this.$element.css({ display: 'inline-block', width: '320px' });

        this.selectedChangeEvent = this.$appState.selected.subscribe("change", e => {
            if (e.field === 'document') {
                if (e.value !== null)
                    this.UpdateModel(e.value);
                else
                    this.model = null;
            }
        })
    }
    $onDestroy(): void {
        this.selectedChangeEvent.unsubscribe();
    }

    ToggleByBarcode(e: JQueryEventObject, barcode: number): void {
        e.preventDefault();
        this["onToggleByBarcode"](barcode);
    }

    private UpdateModel(s: kendo.data.Model): void {
        this.$http({
            method: 'GET',
            url: 'api/Data/DocumentDetails',
            params: { id: s.get("ID"), type: s.get("TYPE") }
        }).then((response: ng.IHttpPromiseCallbackArg<any>) => {
            this.model = response.data[0];
        })
    }
}

export class DocumentDetailsComponent implements ng.IComponentOptions {
    public controller: any = DocumentDetailsController;
    public templateUrl: string = 'templates/document-details.template.html';
    public bindings = {
        onToggleByBarcode: '&'
    }
    public require = {
        app: '^app'
    }
}