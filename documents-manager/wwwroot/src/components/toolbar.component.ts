﻿import * as ng from 'angular';

import { AppController } from '../app.component';
import { AppStateService } from '../services/app-state.service';

export class ToolbarController {
    public $inject = [
        '$element',
        '$appState'
    ];

    public options: any;

    //Scopes
    public app: AppController;
    public appState: AppStateService;

    constructor(private $element: ng.IRootElementService, private $appState: AppStateService) {
        this.appState = $appState;

        this.options = {
            toolbar: {
                items: [
                    { template: 'С:' },
                    { template: $.kendoDom('kendo-date-picker', { kOptions: '$ctrl.options.dateFrom()', kScopeField: '$ctrl.dateFromScope', kNgModel: '$ctrl.appState.filter.dateFrom' }) },
                    { template: 'по:' },
                    { template: $.kendoDom('kendo-date-picker', { kOptions: '$ctrl.options.dateTo()', kScopeField: '$ctrl.dateToScope', kNgModel: '$ctrl.appState.filter.dateTo' }) },
                    { type: 'separator' },
                    { template: 'Поиск:' },
                    { template: '<input type="text" class="k-textbox" ng-keyup="$ctrl.onChangeFilterKeyUp($event)" ng-model="$ctrl.appState.filter.q" style="width:270px;" />' },
                    { template: '<button class="k-button" ng-click="$ctrl.onChangeSearch($event)">Поиск</button>' },
                    { template: '<button class="k-button" ng-click="$ctrl.onChangeFilter($event)">Фильтр</button>' },
                    { template: 'по:' },
                    { template: $.kendoDom('kendo-drop-down-list', { kOptions: '$ctrl.options.search.type()', kScopeField: '$ctrl.searchTypeScope', kNgDelay: '$ctrl.appState', kNgModel: '$ctrl.appState.filter.qfiltertype' }) }
                ]
            } as kendo.ui.ToolBarOptions,

            // Дата "ОТ"
            dateFrom: (): kendo.ui.DatePickerOptions => {
                return {
                    format: "dd.MM.yyyy",
                    change: () => {
                        this.onChangeFilter();
                    }
                } as kendo.ui.DatePickerOptions;
            },

            // Дата "ДО"
            dateTo: (): kendo.ui.DatePickerOptions => {
                return {
                    format: "dd.MM.yyyy",
                    change: () => {
                        this.onChangeFilter();
                    }
                } as kendo.ui.DatePickerOptions;
            },

            search: {
                type: (): kendo.ui.DropDownListOptions => {
                    return {
                        dataSource: this.$appState.qfiltertypes,
                        dataTextField: 'name',
                        dataValueField: 'id',
                        valuePrimitive: true,
                        dataBound: (e: kendo.ui.DropDownListDataBoundEvent) => {
                            e.sender.value(this.$appState.filter.get("qfiltertype"));
                        }
                    } as kendo.ui.DropDownListOptions;
                }
            }
        };
    }

    onChangeSearch(): void {
        this.$appState.filter.set('qissearch', true);
    }
    onChangeFilter(): void {
        this.$appState.filter.set("qissearch", false);
    }
    onChangeFilterKeyUp(e: any): void {
        if (e.keyCode === 13)
            this.onChangeFilter();
    }
}

export class ToolbarComponent implements ng.IComponentOptions {
    public controller: any = ToolbarController;
    public templateUrl: string = 'templates/toolbar.template.html';
    public require = {
        app: '^app'
    }
}