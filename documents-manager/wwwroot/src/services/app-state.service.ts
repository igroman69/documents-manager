﻿import * as ng from 'angular';
import * as UIRouter from '@uirouter/angularjs';

import { ITabObject } from '../models/interfaces';
import { ObservableObject, ObservableObjectEvent, ObservableObjectSetEvent } from '../core/observable';

export class AppStateService {
    public static $inject = [
        '$state',
        '$stateParams'
    ];

    // Фильтр
    //public filter: kendo.data.ObservableObject;
    public filter: ObservableObject;
    // Параметры поиска для списка
    public qfiltertypes: kendo.data.DataSource;
    // Панели табов слева
    public tabs: Array<ITabObject>;
    // Выбранный элемент в списке
    public selected: ObservableObject;

    constructor(private $state: UIRouter.StateService, private $stateParams: UIRouter.StateParams) {

        this.filter = new ObservableObject({
            type: Number($stateParams["type"]),
            dateFrom: kendo.date.today(),
            dateTo: kendo.date.today(),
            q: "",
            qfiltertype: "in_designation",
            qissearch: false
        });
        (this.filter["dateFrom"] as Date).setDate((this.filter["dateFrom"] as Date).getDate() - 7);

        this.selected = new ObservableObject({
            document: null
        });

        this.tabs = [
            { caption: "ВСНРМ" },
            { caption: "ВП от ОГК" },
            { caption: "ДВИ" },
            { caption: "ВМ" }
        ];

        this.qfiltertypes = new kendo.data.DataSource();

        this.filter.subscribe("change", (e: ObservableObjectSetEvent) => {
            if (e.field === "type") {
                $state.go("documents", { type: e.value, id: null, barcode: null, q: null });
            }
        });
    }
}