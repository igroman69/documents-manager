﻿import * as ng from 'angular';

export class DataSourceService {
    public static $inject = [
    ];

    private sources: any = {
        // Основной список документов type: "TreeListDataSource"
        documents: {
            pageSize: 60,
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: { url: "api/Data/Documents" }
            },
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "_ID",
                    parentId: "_PARENT_ID",
                    fields: {
                        "ID": { type: "number" },
                        "PARENT_ID": { type: "number", nullable: true },
                        "TYPE": { type: "number" },
                        "CLASS_ID": { type: "number" },
                        "NUMBERS": { type: "string" },
                        "BARCODE": { type: "number" },
                        "PROJECT_ID": { type: "number" },
                        "NAME": { type: "string" },
                        "DESIGNATION": { type: "string" },
                        "ORDERS": { type: "string" },
                        "ORDERS_ID": { type: "string" },
                        "SORT": { type: "number" },
                        "TYPE_PURCHASE": { type: "string" },
                        "TYPE_PARENT": { type: "number" },
                        "SCAN_DATE": { type: "date" },
                        "CONTRACT": { type: "string" },
                        "NOTIFY": { type: "string" },
                        "ST_REPLACEMENT": { type: "string" },
                        "STATE": { type: "string" },
                        "_ID": { type: "number" },
                        "_PARENT_ID": { type: "number", nullable: true },
                    }
                }
            }
        },

        documentsVM: {
            pageSize: 60,
            serverFiltering: true,
            serverPaging: true,
            serverSorting: true,
            transport: {
                read: { url: "api/Data/Documents" }
            },
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ID",
                    fields: {
                        "ID": { type: "number" },
                        "PROJECT_ID": { type: "number" },
                        "MSG_ID": { type: "number" },
                        "TYPE": { type: "number" },
                        "DATE_AGREEMENT": { type: "date" },
                        "DATE_ISSUED_IN_SERVICE": { type: "date" },
                        "DESIGNATION": { type: "string" },
                        "CN_PROJ_OBOZN": { type: "string" },
                        "REG_NUM": { type: "string" },
                        "REVISION": { type: "string" },
                        "VARIANT": { type: "string" },
                        "BARCODE": { type: "string" },
                        "FILE_NAME": { type: "string" },
                        "LINK": { type: "string" }
                    }
                }
            }
        },

        // Состав документа type: "TreeListDataSource"
        composition: {
            transport: {
                read: { url: "api/Data/DocumentComposition" }
            },
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ID",
                    parentId: "PARENT_ID",
                    fields: {
                        "ID": { type: "number" },
                        "PARENT_ID": { type: "number", nullable: true },
                        "TYPE": { type: "number" },
                        "CLASS_ID": { type: "number" },
                        "NAME": { type: "string" },
                        "DESIGNATION": { type: "string" },
                        "BARCODE": { type: "number" }
                    },
                    expanded: true
                }
            }
        },

        // состав документа ведомости материала
        compositionVM: {
            transport: {
                read: { url: "api/Data/DocumentComposition" }
            },
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ID",
                    parentId: "PARENT_ID",
                    fields: {
                        "ID": { type: "number" },
                        "PARENT_ID": { type: "number", nullable: true },
                        "DATE_AGREEMENT": { type: "date", nullable: true },
                        "DATE_ISSUED_IN_SERVICE": { type: "date", nullable: true },
                        "TYPE": { type: "number" },
                        "DESIGNATION": { type: "string" },
                        "CN_PROJ_OBOZN": { type: "string" },
                        "REG_NUM": { type: "string" },
                        "REVISION": { type: "string" },
                        "VARIANT": { type: "string" },
                        "BARCODE": { type: "string" },
                        "FILE_NAME": { type: "string" },
                        "LINK": { type: "string" }
                    },
                    expanded: true
                }
            }
        },

        // Исключенные документы type: "TreeListDataSource"
        excluded: {
            transport: {
                read: { url: "api/Data/DocumentExcluded" }
            },
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ID",
                    parentId: "PARENT_ID",
                    fields: {
                        "ID": { type: "number" },
                        "PARENT_ID": { type: "number", nullable: true },
                        "SORT": { type: "number" },
                        "TYPE": { type: "number", nullable: true },
                        "CLASS_ID": { type: "number", nullable: true },
                        "NAME": { type: "string" },
                        "DESIGNATION": { type: "string" },
                        "ST_NAME": { type: "string" },
                        "ST_DESIGNATION": { type: "string" },
                        "ORDERS": { type: "string" },
                        "BARCODE": { type: "number" },
                        "NUMBERS": { type: "string" },
                        "SCAN_DATE": { type: "date", nullable: true },
                        "USERNAME": { type: "string" },
                        "REASON": { type: "string" }
                    },
                    expanded: true
                }
            }
        },

        // Аннулированные ВП type: "DataSource"
        canceled: {
            transport: {
                read: { url: "api/Data/DocumentCanceledVP" }
            },
            schema: {
                data: "data",
                total: "total",
                model: {
                    id: "ID",
                    fields: {
                        "ID": { type: "number" },
                        "PARENT_ID": { type: "number", nullable: true },
                        "TYPE": { type: "number", nullable: true },
                        "CLASS_ID": { type: "number", nullable: true },
                        "NUMBERS": { type: "string" },
                        "BARCODE": { type: "number" },
                        "PROJECT_ID": { type: "number" },
                        "NAME": { type: "string" },
                        "DESIGNATION": { type: "string" },
                        "RATE": { type: "number", nullable: true },
                        "COMMENTS": { type: "string" },
                        "ORDERS": { type: "string" },
                        "CREATE_DATE": { type: "date" },
                        "USERNAME": { type: "string" },
                        "TYPE_PURCHASE": { type: "number" },
                        "ST_REPLACEMENT_BY": { type: "number", nullable: true },
                        "ST_REPLACEMENT_INSTEAD": { type: "number", nullable: true },
                        "STATE": { type: "string" },
                        "NOTIFY": { type: "string" },
                        "SCAN_DATE": { type: "date" }
                    },
                }

            }
        }
    };

    constructor() {}

    render<T>(type: { new (c: any): T }, name: string, data?: any): T {
        let c = ng.copy(this.sources[name]);
        if (data)
            c.transport.read.data = data;
        return new type(c);
    }
}