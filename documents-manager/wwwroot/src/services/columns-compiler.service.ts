﻿export class ColumnsCompilerService {
    public static $inject = [
    ];

    private columns: any;
    private columnsSort: Array<Array<string>>;

    constructor() {
        this.columns = {
            'ID': { field: 'ID', title: 'ID' },
            'PARENT_ID': { field: 'PARENT_ID', title: 'PARENT_ID' },
            'TYPE': { field: 'TYPE', title: ' ', template: '<i class="icon-type-#=TYPE#"></i>', width: 70 },
            'CLASS_ID': { field: 'CLASS_ID', title: ' ', template: '<i class="icon-class-#=CLASS_ID#"></i>', width: 35 },
            'NUMBERS': { field: 'NUMBERS', title: 'Номер', width: 75 },
            'BARCODE': { field: 'BARCODE', template: '<a class="filelink" ng-click="$ctrl.CheckFileExists($event)" href="api/Data/DocumentFile?id=#=ID#&type=#=TYPE#&barcode=#=BARCODE#&workshop=#=WORKSHOP#">#=BARCODE#</a>', title: 'Штрихкод', width: 85 },
            'WORKSHOP': { field: 'WORKSHOP', title: 'Цех', width: 50 },
            'PROJECT_ID': { field: 'PROJECT_ID', title: 'PROJECT_ID' },
            'NAME': { field: 'NAME', title: 'Наименование', minResizableWidth: 120 },
            'DESIGNATION': { field: 'DESIGNATION', title: 'Обозначение', minResizableWidth: 120 },
            'ORDERS': { field: 'ORDERS', title: 'ШПЗ', minResizableWidth: 120 },
            'ORDERS_ID': { field: 'ORDERS_ID', title: 'ORDERS_ID' },
            'SORT': { field: 'SORT', title: 'SORT' },
            'TYPE_PURCHASE': { field: 'TYPE_PURCHASE', title: 'TYPE_PURCHASE' },
            'TYPE_PARENT': { field: 'TYPE_PARENT', title: 'TYPE_PARENT' },
            'SCAN_DATE': { field: 'SCAN_DATE', title: 'Дата сканирования', format: '{0:dd.MM.yyyy HH:mm:ss}', width: 130 },
            'CONTRACT': { field: 'CONTRACT', title: 'CONTRACT' },
            'NOTIFY': { field: 'NOTIFY', title: 'Извещение', width: 90 },
            'ST_REPLACEMENT': { field: 'ST_REPLACEMENT', title: 'ST_REPLACEMENT' },
            'STATE': { field: 'STATE', title: 'Состояние', width: 90 },

            'DATE_AGREEMENT': { field: 'DATE_AGREEMENT', title: 'Дата утверждения', format: '{0:dd.MM.yyyy HH:mm:ss}', width: 140 },
            'DATE_ISSUED_IN_SERVICE': { field: 'DATE_ISSUED_IN_SERVICE', title: 'Выдано в службы', format: '{0:dd.MM.yyyy HH:mm:ss}', width: 140 },
            'CN_PROJ_OBOZN': { field: 'CN_PROJ_OBOZN', title: 'Обозначение сборочной<br/>единицы/комплекта', width: 160 },
            'TYPE_VM': { field: 'TYPE', title: ' ', template: '<i class="icon-vm-#=TYPE#"></i>', width: 35  },
            'REG_NUM': { field: 'REG_NUM', title: 'Регистрационный номер ВМ', width: 130 },
            'REVISION': { field: 'REVISION', title: 'Версия', width: 60 },
            'VARIANT': { field: 'VARIANT', title: 'Вариант на заказ', width: 200 },
            'BARCODE_VM': { field: 'BARCODE', template: '<a class="filelink" ng-click="$ctrl.CheckFileExists($event)" href="api/Data/DocumentFile?listtype=3&filename=#=FILE_NAME#">#=BARCODE === null ? FILE_NAME : BARCODE#</a>', title: 'Штрихкод', width: 140 },
        };

        this.columnsSort = [
            ["TYPE", "BARCODE", "WORKSHOP", "NUMBERS", "CLASS_ID", "DESIGNATION", "NAME", "ORDERS", "NOTIFY", "STATE", "SCAN_DATE"],
            ["TYPE", "BARCODE", "CLASS_ID", "DESIGNATION", "NAME", "ORDERS", "NOTIFY", "STATE", "SCAN_DATE"],
            ["TYPE", "BARCODE", "CLASS_ID", "DESIGNATION", "NAME", "ORDERS", "NOTIFY", "STATE", "SCAN_DATE"],
            ["DATE_AGREEMENT", "DATE_ISSUED_IN_SERVICE", "TYPE_VM", "DESIGNATION", "CN_PROJ_OBOZN", "REG_NUM", "REVISION", "VARIANT", "BARCODE_VM"]
        ];
    }

    private modifyColumn<T extends kendo.ui.TreeListColumn>(column: T, idx: number, v: string): T {
        if (idx === 0)
            column["expandable"] = true;
        return column;
    }


    public compile<T>(param: string[] | number): T[]{
        let sortable: string[];
        if (typeof param === 'number') {
            sortable = this.columnsSort[param];
        }
        else if (param instanceof Array) {
            sortable = param;
        }
        return sortable.map<T>((v, i, a) => {
            return this.modifyColumn<T>(this.columns[v] as T, i, v);
        });
    }
}