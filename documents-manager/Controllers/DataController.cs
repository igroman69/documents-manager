﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Newtonsoft.Json;
using documents_manager.Models;
using documents_manager.Models.Repositories;
using System.Data;

// For more information on enabling MVC for empty projects, visit http://go.microsoft.com/fwlink/?LinkID=397860

namespace documents_manager.Controllers
{
    public class DataController : Controller
    {
        private DataManager dm;
        public DataController(DataManager dataManager)
        {
            dm = dataManager;
        }

        [ResponseCache(Duration = 300)]
        public IActionResult Bundle()
        {
            return Content("", "text/javascript");
        }

        public async Task<JsonResult> Documents(int type, DateTime dateFrom, DateTime dateTo, string q, string qfiltertype, bool qissearch, string history,  int take = 0, int skip = 0, int page = 0, int pageSize = 0, List<Dictionary<string, string>> sort = null)
        {
            DataTable data = new DataTable();
            Dictionary<string, string> qfiltertypes = new Dictionary<string, string>();
            Dictionary<string, object> search = new Dictionary<string, object>();
            List<string> searchFields = new List<string>();
            string qfilter = qissearch ? "" : q;
            int total = 0;

            if (type < 3)
            {
                string barcode = Request.Query["barcode"];
                if (String.IsNullOrEmpty(barcode))
                    barcode = "0";

                data = await Task.Run(() => dm.GetRepository<DataRepository>().GetDocuments(type, dateFrom, dateTo, barcode, qfilter, qfiltertype));

                qfiltertypes = new Dictionary<string, string>
                {
                    { "in_orders", "Заказ" }, { "in_designation", "Обозначение" }, { "in_number", "Номер" },
                    { "in_name", "Наименование" }, { "in_barcode", "Штрихкод" }
                };
                searchFields = new List<string> { "BARCODE", "NUMBERS", "DESIGNATION", "NAME", "ORDERS" };
                if (type > 0)
                {
                    qfiltertypes.Remove("in_number");
                    searchFields.RemoveAt(1);
                }

                
            }
            else if (type == 3)
            {
                int id = Convert.ToInt32(Request.Query["id"]);
                data = await Task.Run(() => dm.GetRepository<DataRepository>().GetVMDocuments(id, dateFrom, dateTo, qfilter, qfiltertype));
                qfiltertypes = new Dictionary<string, string>
                {
                    { "in_designation", "Обозначение ВМ" },{ "in_change_designation", "Обозначение ИИ" }, { "in_barcode", "Штрихкод" }, { "in_order_code", "Заказ" }
                };
                searchFields = new List<string> { "DESIGNATION", "BARCODE" };
            }

            if (data.Rows.Count > 0)
            {
                total = data.Rows.Count;
                if (qissearch && !String.IsNullOrEmpty(q))
                {
                    string idColumn = data.Columns.Contains("_ID") ? "_ID" : "ID";
                    string[] or = q.Split('|');
                    int searchPage = page;
                    long[] exluded = new long[0];
                    long[] h = JsonConvert.DeserializeObject<long[]>(history);
                    if (h.Length == 0)
                        searchPage = 1;
                    DataRow founded = data.AsEnumerable().Skip((searchPage - 1) * pageSize).FirstOrDefault(x =>
                    {
                        //string[] values = new string[] { x["DESIGNATION"]?.ToString().ToLower() };
                        string[] values = searchFields.Select(s => x[s]?.ToString().ToLower()).ToArray();
                        return !h.Contains(Convert.ToInt64(x[idColumn])) && or.Count(o =>
                        {
                            string[] and = o.Split(' ');
                            return and.Count(a => values.Count(v => v.Contains(a.ToLower())) > 0) == and.Length;
                        }) > 0;
                    });

                    if(founded != null)
                    {
                        int foundIdx = data.AsEnumerable().ToList().FindIndex(x => Convert.ToInt64(x[idColumn]) == Convert.ToInt64(founded[idColumn]));
                        search.Add("fields", searchFields);
                        search.Add("founded", Convert.ToInt64(founded[idColumn]));
                        search.Add("page", (int)Math.Ceiling(((double)foundIdx + 1) / (double)pageSize));
                        search.Add("redirect", page != (int)search["page"]);
                    }
                    else
                    {
                        if (h.Length > 0)
                            search.Add("message", $"Поиск завершен! Кол-во найденых совпадений {h.Length}.");
                        else
                            search.Add("message", "Ничего не найдено!");
                        //search.Add("message", "Not Found");
                    }
                }
                data = data.AsEnumerable().Skip((page - 1) * pageSize).Take(pageSize).CopyToDataTable();
            }

            List<Dictionary<string, string>> _qfiltertypes = new List<Dictionary<string, string>>();
            _qfiltertypes = qfiltertypes.Select(p =>
            {
                return new Dictionary<string, string> { { "id", p.Key }, { "name", p.Value } };
            }).ToList();

            if(search.Keys.Count == 0)
                search = null;

            return Json(new { total = total, qfiltertypes = _qfiltertypes, search = search, data = data }, new JsonSerializerSettings());
        }

        public async Task<JsonResult> DocumentDetails(int id, int type)
        {
            DataTable result = await Task.Run(() => dm.GetRepository<DataRepository>().GetDocumentExtra(id, type));
            return Json(result, new JsonSerializerSettings());
        }

        public async Task<ActionResult> DocumentFile(int id, int type, string filename, string workshop, int barcode)
        {
            try
            {
                string fileName = filename;
                byte[] content = new byte[0];
                if (Convert.ToInt32(Request.Query["listtype"]) == 3)
                {
                    using(System.Net.WebClient wc = new System.Net.WebClient())
                    {
                        wc.UseDefaultCredentials = true;
                        content = wc.DownloadData($"http://app05.tm.local/vedomost/file.ashx?filename={fileName}");
                    }
                }
                else
                {
                    content = await Task.Run(() => dm.GetRepository<DataRepository>().GetDocumentFile(id, type, barcode, workshop, out fileName));
                }
                return File(content, "application/octet-stream", fileName);
            }
            catch (System.IO.FileNotFoundException ex)
            {
                return StatusCode(404);
            }
            catch (System.Net.WebException ex)
            {
                return StatusCode(404);
            }
        }

        public async Task<JsonResult> DocumentComposition(int id = 0, int type = 0, int project_id = 0, bool is_vm = false)
        {
            if ((id == 0 && type == 0) || project_id == 0)
                return Json("{total: 0, data: [] }", new JsonSerializerSettings());

            DataTable result = new DataTable();
            if (!is_vm)
            {
                result = await Task.Run(() => dm.GetRepository<DataRepository>().GetDocumentComposition(id, type));
            }
            else
            {
                result = await Task.Run(() => dm.GetRepository<DataRepository>().GetDocumentVMComposition(project_id));
            }
            return Json(new { total = result.Rows.Count, data = result }, new JsonSerializerSettings());
        }

        public async Task<JsonResult> DocumentExcluded(int id = 0, int type = 0)
        {
            DataTable result = await Task.Run(() => dm.GetRepository<DataRepository>().GetDocumentExcludes(id, type));
            return Json(new { total = result.Rows.Count, data = result }, new JsonSerializerSettings());
        }

        public async Task<JsonResult> DocumentCanceledVP(int id = 0, int type = 0)
        {
            if (id == 0 && type == 0)
                return Json("{total: 0, data: [] }", new JsonSerializerSettings());
            DataTable result = await Task.Run(() => dm.GetRepository<DataRepository>().GetDocumentCanceledVP(id, type));
            return Json(new { total = result.Rows.Count, data = result }, new JsonSerializerSettings());
        }
    }
}
