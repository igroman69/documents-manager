﻿using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Microsoft.AspNetCore.Mvc.Filters;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using documents_manager.Models;

namespace documents_manager.Core
{
    public class DomainAuthorizationAttribute : IAsyncAuthorizationFilter
    {
        public async Task OnAuthorizationAsync(AuthorizationFilterContext filterContext)
        {
            if (!filterContext.HttpContext.Session.Keys.Contains("UserId"))
            {
                int user_id = 0;

                await Task.Run(() => Int32.TryParse(filterContext.HttpContext.User.Identity.Name.Replace(@"TM\", ""), out user_id));

                if (user_id == 0)
                    filterContext.Result = new ContentResult { Content = "Нет доступа!" };
                else
                    filterContext.HttpContext.Session.SetInt32("UserId", user_id);
            } // end if
        }
    }
}